1) Install PHP Swoole 

 * sudo apt-get update && sudo apt-get upgrade
 * sudo apt install php-dev   
 * sudo pecl install swoole

2) Install SSL (ex. by certbot) 
3) Config NGINX EXAMPLE
***
    server {
    server_name DOMAIN.COM;
    root /var/www/SwooleChat;

    location / {
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/(index)\.php(/|$) {
        fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
    }

    location /wss/ {
    proxy_pass http://127.0.0.1:9502;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    }

    
***
4) install DB and export dump DB (chat.sql)
5) set config (config.json)
***
    {
    "mysqlServer": "localhost",
    "mysqlPort": 3306,
    "mysqlDatabaseName": "chat",
    "mysqlLogin": "root",
    "mysqlPassword": 123456,
    "swooleServer": "127.0.0.1",
    "swoolePort": 9503
    }
***

6) run command **php swoole.php** or using nohup (  **nohup php swoole.php &**  )