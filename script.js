var search = location.search.substring(1);
var params = JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')

document.getElementById('username').value = params.username;
document.getElementById('room').value = params.room;


var clientInformation = {
    username: document.getElementById('username').value
    // You can add more information in a static object
};


var Chat = {
    appendMessage: function(message){

        if (message.username == '--!'){
            $('#countusers').html('( Онлайн:'+message.txt +'чел. ) ');
            return true;
        }

        if (message.txt == 'DELETEMESSAGE'){
            $('.message-container[data-id="'+message.id+'"]').remove();
        }else{
            if (message.room == document.getElementById('room').value){
                console.log(document.getElementById('username').value)
                console.log(message.username);

                if (document.getElementById('isedit') != null && document.getElementById('isedit').value == '1'){
                    var re = '<div style="text-align: right">' +
                        '<div class="message-comment"><i>'+message.comment+'</i></div>' +
                        ' <span class="btn btn-danger remove-message"  data-id="'+message.id+'">Удалить</span>' +
                        ' <span class="btn btn-primary answer-message"  data-id="'+message.id+'">Ответить</span>' +
                        ' <span class="btn btn-success success-message"  data-id="'+message.id+'">Разрешить</span>' +
                        '</div>';
                }else{
                    var re = '';
                }
                if (message.username === document.getElementById('username').value){
                    var aself = 1;
                }else{
                    var aself = 0;
                }

                var msgHTML = '<div class="message-container" data-id="'+message.id+'">\n' +
                    '            <div class="message-author">' +
                    '             <div class="message-name">'+message.username+'</div>\n' +
                    '            <div class="message-date">'+message.date+'</div>\n' +
                    '            </div>' +
                    '           <div class="message-content">\n' +
                    message.txt + re +
                    '            </div>\n' +
                    '        </div>';


                var bb = $('.chat-container');
                $(bb).prepend(msgHTML);
            }
        }


    },
    sendMessage: function(text, room, parentId){
        parentId = typeof parentId !== 'undefined' ?  parentId : 0;
        clientInformation.message = {
            txt: text, username: document.getElementById('username').value,
            room: room,
            email: document.getElementById('email').value,
            mach: document.getElementById('mach').value,
            parentId: parentId
        };
        conn.send(JSON.stringify(clientInformation.message));
    },
    successMessage: function(username, text, r, id){
        clientInformation.message = {
            txt: text,
            username: username,
            room: r,
            email: username,
            id: id,
            mach: document.getElementById('mach').value
        };

        console.log(clientInformation.message);

        conn.send(JSON.stringify(clientInformation.message));
    },

    removeMessage: function (id){
        clientInformation.message = {
            txt: 'DELETEMESSAGE',
            id: id
        };
        conn.send(JSON.stringify(clientInformation.message));
    }
};


function connect() {

    conn = new WebSocket('wss://DOMAIN.RU/wss/?email='+document.getElementById('email').value + '&eventId=' + document.getElementById('eventId').value + '&userId=' + document.getElementById('userId').value);


    // START SOCKET CONFIG
    /**
     * Note that you need to change the "sandbox" for the URL of your project.
     * According to the configuration in Sockets/Chat.php , change the port if you need to.
     * @type WebSocket
     */


    conn.onopen = function (e) {
        console.info("Connection established succesfully");
    };

    conn.onmessage = function (e) {
        var data = JSON.parse(e.data);
        console.log(data);

        Chat.appendMessage(data);

        console.log(data);
        return false;
    };


    conn.onerror = function (e) {
        console.log(e);
        conn.close();
    };
    // END SOCKET CONFIG

    conn.onclose = function (e) {
        console.log('Произошла ошибка чата. Перезугрузите, пожалуйста, страницу', e.reason);

        setTimeout(function () {
            window.location.reload();
        }, 5000);
    };

}

$('body').on("click", "#message-send", function (){
    var msg = document.getElementById("chat-input").value;

    if (!msg || msg.length < 3) {
        alert("Для отправки сообщения оно не может содержать менее 3 символов");
    }
    if (document.getElementById('isedit') != null && document.getElementById('isedit').value == '1') {
        Chat.sendMessage(msg,  document.getElementById('room').value, $('#answerId').val());
    }else{
        if (parseInt(document.getElementById('room').value) > 8000){
            Chat.sendMessage(msg,  parseInt(document.getElementById('room').value)+'m', null);
        }else{
            Chat.sendMessage(msg,  document.getElementById('room').value, null);
        }
    }
    document.getElementById("chat-input").value = "";
});


$('body').on("click", ".remove-message", function (){
    var id = $(this).attr('data-id');
    Chat.removeMessage(id);
});

$('body').on("click", ".success-message", function (){
    var id = $(this).attr('data-id');
    console.log($(this).closest('.message-container'));
    var name = $(this).closest('.message-container').find('.message-name').html();
    var r = parseInt(document.getElementById('room').value);
    var txt = $(this).closest('.message-container').find('.message-content');
    $(txt).find('div').remove();

    Chat.successMessage(name,$(txt).html(), r,id);
    var id = $(this).attr('data-id');
    // Chat.removeMessage(id);

});

$('body').on("click", ".answer-message", function (){
    $('#answerId').val($(this).attr('data-id'));
});


$('body').on("click", ".send-message", function (){
    var id = $(this).attr('data-id');
    var sid = $(this).attr('data-sid');
    console.log($(this).closest('.message-container'));
    var name = $(this).closest('.message-container').find('.message-name').html();
    var r = parseInt(document.getElementById('room').value);
    var txt = $(this).closest('.message-container').find('.message-content');
    var btns = $(txt).find('div').html();
    $(txt).find('div').remove();

    Chat.successMessage(name,$(txt).html(), r+'s'+sid, id);
    $(txt).append('<div style="text-align: right"><div class="message-comment"><i>Отправлен в тему '+sid+'</i></div>'+btns+'</div>');
    var id = $(this).attr('data-id');
});



connect();
