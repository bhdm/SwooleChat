<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);


use Swoole\WebSocket\Server;
use Swoole\Http\Request;
use Swoole\WebSocket\Frame;

$time = time();
$config = json_decode(file_get_contents("config.json"));
$mysqli = new mysqli($config['mysqlServer'], $config['mysqlLogin'], $config['mysqlPassword'], $config['mysqlDatabaseName']);
$mysqli->set_charset('utf8');


$server = new Server("0.0.0.0", $config['swoolePort']);

$server->on("start", function (Server $server) {
    echo "Swoole WebSocket Server is started \n";
});

$server->on('open', function(Server $server, Swoole\Http\Request $request ) {
    echo "connection open: {$request->fd}\n";
    $server->tick(30000, function() use ($server, $request) {
        @$server->push($request->fd, json_encode(['username'=> '--!', 'txt' => $server->stats()["connection_num"], 'date'=> (new \DateTime())->format('H:i')]));


        if ($request->get['email']){
            $mysql = new Swoole\Coroutine\MySQL();

//            print_r("ping ".$request->get['email']."\r\n");
            // connect
            $mysql->connect([
                'host' => 'localhost',
                'port' => 3306,
                'user' => 'root',
                'password' => 'A3245897a',
                'database' => 'chat',
            ]);

            $eventId = (isset($request->get['eventId']) ? $request->get['eventId'] : 0);
            $userId = (isset($request->get['userId']) ? $request->get['userId'] : 0);
            $sql = "INSERT INTO `ping_log` (`id`, `userId`, `email`,  `amount_ping`, `eventId`) VALUES (NULL, '".$userId."', '".$request->get['email']."', '1', '".$eventId."');";

            $mysql->setDefer(true);
            $result = $mysql->query($sql);
            var_dump($result);
        }


    });

});


$server->on('message', function(Server $server, Frame $frame) {
    echo "received message: {$frame->data}\n";

    $data = $frame->data;
    $data = json_decode($data, true);
    print_r($data);
    $time = time();
    $mysqli = new mysqli("localhost", "root", "A3245897a", "chat");
    $mysqli->set_charset('utf8');
    $comment = ' ';

    if (is_int($data['room'])){ $comment = 'Отправлен в Чат '; }

    if ($data['txt'] != 'DELETEMESSAGE'){
        if (!$mysqli->connect_errno) {

            if (!isset($data['parentId']) || $data['parentId'] == ''){
                $data['parentId'] = null;
                $data['parentId'] = null;
            }

            $sql = "INSERT message set username = \"" . $data['username'] . "\", content = \"" . addslashes($data['txt']) . "\", 
 		created = \"" . (new \DateTime())->format('Y-m-d H:i:s') . "\", room='".$data['room']."', `time`=".$time.", `email`=\"".$data['email']."\", 
 		`mach`=\"".$data['mach']."\", parentId='".$data['parentId']."'; 		
 		";

            echo "\r\n INSERT \r\n".$sql."\r\n";

            print_r($mysqli->query($sql));

            if (isset($data['id'])){
                $sql = "UPDATE  `message` SET `comment` = \"".$comment."\" WHERE `time`= ".$data['id'];
                echo $mysqli->query($sql);
                echo "\r\n". $sql. "\r\n";
            }else{
                echo "\r\n NO ID \r\n";
            }
        }
        if ($data['txt'] != ''){
            if ($data['parentId'] != null){
                $sql = "SELECT * FROM message WHERE time='".$data['parentId']."' ORDER BY id DESC LIMIT 1";
                $question = $mysqli->query($sql)->fetch_assoc();
                $q = '<div class="message-container" style="background: #f1f9ff">
                    <div class="message-author" style="background: #f1f9ff">
                        <div class="message-name" style="background: #f1f9ff">'.$question['username'].'</div>
                    </div>
                    <div class="message-content" style="background: #f1f9ff">'.$question['content'].'</div>
                </div>';
            }else{
                $q = '';
            }

            foreach ($server->connections as $conn)
                $server->push($conn, json_encode(
                    [
                        'username'=> $data['username'],
                        'txt' => $q . $data['txt'],
                        'date'=> (new \DateTime())->format('H:i'),
                        'room'=>$data['room'],
                        'id' => $time,
                        'comment' => $comment,
                    ]));
        }
    }else{
        if (!$mysqli->connect_errno) {
            $sql = "DELETE FROM message WHERE time= ".$data['id'];
            $mysqli->query($sql);
            foreach ($server->connections as $conn){
                $server->push($conn, json_encode(
                    [
                        'txt'=> 'DELETEMESSAGE',
                        'id' => $data['id'],
                    ]));
            }
        }
    }

});

$server->on('close', function(Server $server, int $fd) {
    echo "connection close: {$fd}\n";
});

$server->start();

