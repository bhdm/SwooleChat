<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
?>
<!DOCTYPE html>
<html lang="ru" style="    overflow-y: hidden;">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic" media="all">
    <link rel="stylesheet" href="assets/less/common2.css?3">


    <title>ЧАТ</title>

</head>
<body style="padding: 10px; background: #F1F9FF; ">
<?php
$config = json_decode(file_get_contents("config.json"));

$mysqli = new mysqli($config['mysqlServer'], $config['mysqlLogin'], $config['mysqlPassword'], $config['mysqlDatabaseName']);
$mysqli->set_charset('utf8');


if (!$mysqli->connect_errno) {
    $sql = "SELECT * FROM message WHERE room='".$_GET['room']."' ORDER BY id DESC LIMIT 50";
//    var_dump($sql);
    $res = $mysqli->query($sql);
}
?>
<div id="chat-wrapper" style="background: #F1F9FF; width: <?php echo $_GET['w'] ?>px; height: <?php echo $_GET['h'] ?>px" >
    <div class="" style="padding: 10px 20px; color: #333"><span style="font-weight: 400"><?php $_GET['isedit']=='1' ? print_r('<span id="countusers"></span>') : '' ?>Сообщения:</span></div>
    <div id="chat" style="height: <?php echo ($_GET['h']-120) ?>px">
        <div class="chat-container">
            <?php
            while ($row = $res->fetch_assoc()) {
                if ($row['content'] != ''){

                    if ($row['parentId'] != null){
                        $sql = "SELECT * FROM message WHERE time='".$row['parentId']."' ORDER BY id DESC LIMIT 1";
                        $question = $mysqli->query($sql)->fetch_assoc();
                        $q = '<div class="message-container" style="background: #f1f9ff">
                            <div class="message-author" style="background: #f1f9ff">
                                <div class="message-name" style="background: #f1f9ff">'.$question['username'].'</div>
                            </div>
                            <div class="message-content" style="background: #f1f9ff">'.$question['content'].'</div>
                        </div>';
                    }else{
                        $q = '';
                    }

                ?>
                <div class="message-container" data-id="<?php echo $row['time'] ?>">
                    <div class="message-author">
                        <div class="message-name"><?php echo $row['username'] ?></div>
                        <div class="message-date"><?php echo (new \DateTime($row['created']))->format('H:i') ?></div>

                    </div>
                    <div class="message-content">
                        <?php echo $q . $row['content'] ?>
                        <?php if ($_GET['isedit']==1){ ?>
                            <div style="text-align: right">

                                <div class="message-comment"><i><?php echo $row['comment'] ?></i></div>
                                <span  style="cursor: pointer" class="btn btn-danger  remove-message" data-id="<?php echo $row['time'] ?>">Удалить</span>
                                <span  style="cursor: pointer" class="btn btn-primary  answer-message" data-id="<?php echo $row['time'] ?>">Ответить</span>
                                <span  style="cursor: pointer" class="btn btn-success  success-message" data-id="<?php echo $row['time'] ?>">Разрешить</span>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <?php
                }
            }
            ?>


        </div>
    </div>
    <div class="chat-form">
        <input type="hidden" id="username" value="">
        <input type="hidden" id="email" value="<?php echo $_GET['email'] ?>">
        <input type="hidden" id="userId" value="<?php echo $_GET['userId'] ?>">
        <input type="hidden" id="mach" value="<?php echo $_GET['machash'] ?>">
        <input type="hidden" id="room" value="1">
        <input type="hidden" id="eventId" value="<?php echo $_GET['eventId'] ?>">

        <div class="textarea-box">
             <textarea id="chat-input" maxlength="1000"></textarea>
            <?php if ($_GET['isedit']==1){ ?>
            ID вопроса: <input type="text" name="answerId" id="answerId">
            <?php } ?>
            <div class="message-send">
                <div id="message-send" style="width: 200px; padding: 5px">
                    Отправить сообщение
                </div>
            </div>
            <br style="clear: both"/>
        </div>
        <?php if ($_GET['isedit']==1){ ?>
            <input type="hidden" id="isedit" value="1">
        <?php } ?>
        <?php if ($_GET['isspieker']==1){ ?>
            <input type="hidden" id="isspieker" value="1">
        <?php } ?>
    </div>
</div>
<script src="assets/jquery/dist/jquery.min.js"></script>
<script src="script.js?10"></script>
</body>
</html>